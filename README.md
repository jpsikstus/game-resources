# Game Resources

## Tilesets

- [16x16 mini world sprites](https://merchant-shade.itch.io/16x16-mini-world-sprites) - Small overworld tileset
- [pixel art top down basic](https://cainos.itch.io/pixel-art-top-down-basic) - Simple grassy tileset with props
- [12x12 rpg tileset](https://cypor.itch.io/12x12-rpg-tileset) - Simple cute tileset
- [forest_](https://analogstudios.itch.io/forest) - Small simple forest tileset (part of collection)
- [Overgrown memories](https://tomthebbq.itch.io/overgrown-memories) - Simple grassy tileset
- [Serene village revamped](https://limezu.itch.io/serenevillagerevamped) - Village tileset

## Sprites

- [Pokemon sprites](https://www.pokencyclopedia.info/en/index.php?id=sprites) - Collection of pokemon sprites, including overworld
- [Witch pack](https://9e0.itch.io/witches-pack) - 3 animated witches

## Misc

- [Input Prompts](https://kenney.nl/assets/input-prompts-pixel-16) - Pixel art input prompt images
